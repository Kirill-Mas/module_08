terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "y0_AgAAAABqT1WnAATuwQAAAADiI_aB6YwmclAmR9aXAPWsDf3Bkut5-lk"
  cloud_id  = "b1go4n53hofro0ufj8ji"
  folder_id = "b1gj2tad8qm148k1ihre"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "my-vm-1" {
  name        = "test-vm-1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8auu58m9ic4rtekngm"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat = true
  }

  metadata = {
    user-script = "ubuntu:${file("/home/kirill/homework9/module_08/terraform/user_data.sh")}"
    ssh-keys = "ubuntu:${file("/home/kirill/.ssh/id_ed25519.pub")}"
  }
}

resource "yandex_compute_instance" "my-vm-2" {
  name        = "test-vm-2"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8auu58m9ic4rtekngm"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat = true
  }

  metadata = {
    user-script = "ubuntu:${file("/home/kirill/homework9/module_08/terraform/user_data.sh")}"
    ssh-keys = "ubuntu:${file("/home/kirill/.ssh/id_ed25519.pub")}"
  }
}

resource "yandex_compute_instance" "my-vm-3" {
  name        = "test-vm-3"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8auu58m9ic4rtekngm"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat = true
  }

  metadata = {
    user-script = "ubuntu:${file("/home/kirill/homework9/module_08/terraform/user_data.sh")}"
    ssh-keys = "ubuntu:${file("/home/kirill/.ssh/id_ed25519.pub")}"
  }
}

resource "yandex_vpc_network" "my-nw-1" {
  name = "my-nw-1"
}

resource "yandex_vpc_subnet" "my-sn-1" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.my-nw-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.nat_ip_address
}

output "internal_ip_address_vm_2" {
  value = yandex_compute_instance.my-vm-2.network_interface.0.ip_address
}

output "external_ip_address_vm_2" {
  value = yandex_compute_instance.my-vm-2.network_interface.0.nat_ip_address
}

output "internal_ip_address_vm_3" {
  value = yandex_compute_instance.my-vm-3.network_interface.0.ip_address
}

output "external_ip_address_vm_3" {
  value = yandex_compute_instance.my-vm-3.network_interface.0.nat_ip_address
}

